import React from 'react';
import { Button } from 'react-bootstrap';
import ModalButton from '../../../generics/Modal';

function DetailsSection({details,user}) {
    return(
        <div>
            
             {
                Object.keys(details).map((detail)=>{
                    return(
                    <li key={detail}>{detail} : {details[detail]}</li>
                    )
                })
            } 
            
            <ModalButton 
                header={`More details about ${user.name}`}
                data={user.details}
                btnText='Show on same page' />
            <Button>Show on New Page</Button>
        </div>
    )
}

export default DetailsSection;