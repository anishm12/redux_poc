import React, { useContext,useEffect, useState } from 'react'
import {ExportContext} from '../index'
import {  Button, InputGroup, Form } from 'react-bootstrap'
import {createWorkbookAndSave} from '../export_utils.js'


 

export default function ExportBody(props) {
    const exportContext=useContext(ExportContext)
    const [sheets,setSheets]=useState([])
    useEffect(()=>{
        console.log(exportContext.filtered_data)
    },[])
    

        return (
             <div>
                <Form>
                
                <InputGroup onChange={(e)=>console.log(e.target.form)} >
                {
                    Object.keys(exportContext).map((sheet)=>{
                        return(
                            
                            <InputGroup.Text key={sheet}>
                                {sheet}
                                <InputGroup.Checkbox 
                                value={sheet} 
                                onChange={(e)=>{
                                    if(e.target.checked)
                                    {
                                        //add sheet
                                        setSheets([...sheets,exportContext[sheet]])
                                        
                                    }
                                    else{
                                        //remove sheet
                                        const sheetIndex=sheets.indexOf(exportContext[sheet])
                                        setSheets([...sheets.slice(0,sheetIndex),...sheets.slice(sheetIndex+1,sheets.length)])
                                    }
                                }}  />
                            </InputGroup.Text>
                                     
                        )
                    })
                }
                </InputGroup>
                </Form><br/>
                <Button onClick={()=>createWorkbookAndSave(sheets,props.table_headers,props.mode)}>Download</Button>
                {
                    Object.values(sheets).map((sheet)=>{
                    return(<>{sheet.length}</>)
                    })
                }
                </div>
             
        )
   
}
