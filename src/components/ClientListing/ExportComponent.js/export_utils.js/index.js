import xlsx from 'xlsx'
import {saveAs} from 'file-saver'
import JSzip from 'jszip'

 

function saveFile(workbook,mode) {
    
    const wb_blob=createBlob(workbook,'xlsx')
    //In excel
    if(mode==='excel')
    {
        saveAs(wb_blob, "test.xlsx");
    
    }
    //In zip
    else if(mode==='zip')
    {
        
        const jszip=new JSzip()
        const xlsFolder = jszip.folder("xlszip");
        
        addToZip(xlsFolder,wb_blob,'filtered.xlsx')
        
        jszip.generateAsync({ type: "blob" }).then(function(content) {
            // see FileSaver.js
            saveAs(content, "data.zip");
        });
    }
}

function createBlob(workbook,type) {
    var wopts = { bookType: type, bookSST: false, type: "array" };
    try {
        var wbout = xlsx.write(workbook, wopts);
        const wb_blob=new Blob([wbout],{ type: "application/octet-stream" })
        return wb_blob
    } catch (err) {
        console.error(err);
        
    }
    

}





function addToZip(zip_folder,wb_blob,file_name) {
    zip_folder.file(file_name,wb_blob)
}

function createWorksheet(ws_data,attrs) {
    
    const ws = xlsx.utils.json_to_sheet(ws_data);
    return ws
    
}

function createWorkbook(ws_data,attrs) {
    var wb = xlsx.utils.book_new(); 
    var ws = xlsx.utils.json_to_sheet(ws_data);
    xlsx.utils.book_append_sheet(wb, ws, "Sheet 1");
    return wb
}

export function createWorkbookAndSave(data,attrs,mode) {
    var wb = xlsx.utils.book_new();
     
    
    data.map((sheet_data,sheet_index)=>{
        var ws = xlsx.utils.json_to_sheet(sheet_data);
        xlsx.utils.book_append_sheet(wb, ws, `Sheet ${sheet_index}`);
    })
    
    try {
        saveFile(wb,mode)
    } catch (err) {
        console.log(err)
    }
}

function addSheetToWorkBook(workbook,worksheet,sheet_name) {
    xlsx.utils.book_append_sheet(workbook,worksheet,sheet_name)
}

function removeSheetFromWorkbook() {
    
}

export function createZipAndSave() {
    
}


