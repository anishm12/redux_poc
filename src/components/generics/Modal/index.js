import React, { useState ,useRef, useCallback, useEffect} from 'react';
import {Modal, Button, Tabs,Tab,Sonnet, Spinner} from 'react-bootstrap'
import { Nav } from 'react-bootstrap';
import axios from 'axios';



function ModalButton({btnText,data}) {
      
    const [show,setShow]=useState(false)
    const [aggr,setAggr]=useState(undefined)
    
    const handleVisibility=useCallback(
        vis=>{
            setShow(vis)
        }
    ,[setShow])
    
    useEffect(()=>{
        if(show)
        {
            
            axios.get('http://www.mocky.io/v2/5e0d99d53300004d00aa8712')
            .then((resp)=>{
                setAggr({...data,...resp.data[data['location_id']-1]})
                console.clear()
            })
            .catch((err)=>console.log(err))
        }
        else
        {
             
            console.log('hidden')
        }
    },[show])

    return(
        <>
            <Button onClick={()=>handleVisibility(true)}>
                {btnText}
            </Button>
            {/* Add modal to seperate component */}
            {show?
                <Modal show={show} onHide={()=>handleVisibility(false)} >

                 
                  
                
               <Modal.Body>

               
                {
                    aggr!==undefined?
                    Object.keys(aggr).map((aggr_prop,aggr_prop_index)=>{
                    return(
                            <p key={aggr_prop_index} >{aggr_prop} : {aggr[aggr_prop]}</p>
                        )
                    }) 
                    :
                    <center><Spinner animation='border' /></center>
                 }
               </Modal.Body>
               <Modal.Footer>
               
                    <Button onClick={()=>handleVisibility(false)}>Close</Button>
                
               </Modal.Footer>
            </Modal>
            :
            null
            }
        </>
    )    
}

export default ModalButton;